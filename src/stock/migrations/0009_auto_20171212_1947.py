# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-12 19:47
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0008_auto_20171211_1922'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockdetails',
            name='date',
            field=models.DateField(default=datetime.date(2017, 12, 12)),
        ),
    ]
