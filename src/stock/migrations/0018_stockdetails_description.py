# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-01-27 18:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0017_auto_20180127_1812'),
    ]

    operations = [
        migrations.AddField(
            model_name='stockdetails',
            name='description',
            field=models.TextField(blank=True, default='', null=True),
        ),
    ]
