# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0019_remove_stockdetails_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockdetails',
            name='date',
            field=models.DateField(default=datetime.date(2018, 2, 3)),
        ),
    ]
