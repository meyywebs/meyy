# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import render,Http404
from django.shortcuts import redirect
from django.contrib import messages
from django.db import IntegrityError, transaction
from django.forms.formsets import formset_factory
from models import ItemDetails
from models import StockDetails
from bootstrap_datepicker.widgets import DatePicker
from django.forms.models import modelformset_factory
from django.core.exceptions import ObjectDoesNotExist
from .forms import ItemDetailsForm,StockDetailsForm
from .forms import ItemDetailsEditForm,StockDetailsEditForm
from operator import attrgetter
from decimal import Decimal
#from django.db.models import FilteredRelation, Q
import random
import datetime
import calendar
import re
from datetime import date,timedelta
from django.db.models import Sum
from newsletter.utils import render_to_pdf
# Create your views here.
from datetime import timedelta, date

def daterange(date1, date2):
    for n in range(int ((date2 - date1).days)+1):
        yield date1 + timedelta(n)

def stock_home(request):
	if request.user.is_authenticated() and request.user.is_staff:
		form = ItemDetailsForm(request.POST or None)
		if form.is_valid():
			if not request.user.is_active:
				return redirect('home')
			form.save()
			itemname = form.cleaned_data.get('name')
			stockrecord = StockDetails(date=datetime.datetime.now(),
										itemname=ItemDetails.objects.get(name=itemname),
										opening=form.cleaned_data.get('opening'))
			stockrecord.save()
			messages.success(request, 'successfully added!')
			
		queryset = ItemDetails.objects.all().order_by('-timestamp')
		context = {
			"logged_in_as": "You are logged in as admin",
			"form" : form,
			"queryset":queryset
		}
		return render(request,"stock/home.html",context)
	else:
		raise Http404

def item_edit(request,param): 
	if request.user.is_authenticated() and request.user.is_staff:
	 	itemdetail = ItemDetails.objects.get(name=param)
		form = ItemDetailsEditForm(request.POST or None ,initial=itemdetail.details())
		if 'delete' in request.POST:
			if form.is_valid():
				if not request.user.is_active:
					return redirect('home')
				itemdetail.delete()
				messages.success(request, 'successfully deleted!')
				return redirect('stock_home')
		else :
			if form.is_valid():
				if not request.user.is_active:
					return redirect('home')
				itemdetail.description = form.cleaned_data.get('description')
				itemdetail.itemtype = form.cleaned_data.get('itemtype')
				itemdetail.kdm = form.cleaned_data.get('kdm')
				itemdetail.active = form.cleaned_data.get('active')
				itemdetail.opening = form.cleaned_data.get('opening')
				# if itemdetail.itemtype == 'S':
				# 	#form.cleaned_data.set('kdm',False)
				# 	itemdetail.kdm = False
				# 	itemdetail.save()
				# #item_instance =  ItemDetails.objects.get(name=itemname)
				# else :
				itemdetail.save()
				try:
					stockdetail = StockDetails.objects.filter(itemname=itemdetail.name).earliest('date')
					stockdetail.opening = itemdetail.opening
					stockdetail.save()
				except:
					pass
				messages.success(request, 'successfully edited item details')
				messages.warning(request, 'if opening is edited please edit the stock from the beginning!')
				return redirect('stock_home')
		context = {
				"logged_in_as": "You are logged in as admin",
				"form" : form,
				"itemname":param,
				  }
		#return Http404tpResponse("param {0}".format(param))
		#return render(request,"stock/edit.html",context)#HttpResponse("stock/edit.html")
		return render(request,"stock/edit.html",context)
	else:
		raise Http404

def daily_stock(request,param=1,date=str(datetime.date.today())):
	if request.user.is_authenticated() and request.user.is_staff:
		try:
			need_date = StockDetails.objects.latest('date')
			need_dt = need_date.date+datetime.timedelta(days=1)
		except ObjectDoesNotExist:
			need_dt = datetime.date.today()
		today = datetime.date.today()
		req_dt = datetime.date(*map(int,date.split('-')))
		request.session['date_updated'] = date
		if req_dt > today :
			messages.warning(request,'future cannot be updated')
			return redirect('home')

		if (need_dt != req_dt) or (req_dt != today):
			if (need_date.date >= req_dt) and (req_dt != today):
				messages.warning(request,str(req_dt)+' has been updated already!')
				return redirect('home')
			if (need_dt < req_dt):
				messages.warning(request,str(need_dt)+' is needed to be updated!')
				return redirect('home')	

		
		queryset = StockDetails.objects.filter(date=date)
		if int(param)<=len(queryset):
			extra = 1
		if int(param)>len(queryset):
			extra = int(param)-len(queryset)

		StockDetailsFormSet = modelformset_factory(StockDetails,form=StockDetailsForm,extra=extra,can_delete=True)
		FormSet = StockDetailsFormSet(request.POST or None, queryset=queryset, initial=[{'date': date,'incoming':'','sales':''} for x in range(len(queryset)+extra)])

		if FormSet.is_valid():
			if not request.user.is_active:
				return redirect('home')
			itemlist = []
			for forms in FormSet:
				item = forms.cleaned_data.get('itemname')
				if item not in itemlist:
					itemlist.append(item)
				else:
					messages.warning(request, str(item) +' has more than 1 record!')
					return redirect('daily_stock',extra,date)
			FormSet.save()
			messages.success(request, 'successfully updated!')
			return redirect('home')
		# else:
		# 	FormSet = StockDetailsFormSet(request.POST or None,queryset=queryset,initial=[{'date': date,'incoming':'','sales':''}])
		
		context = {
			"logged_in_as": "You are logged in as admin",
			"formset" : FormSet,
			"extra" : len(queryset)+extra+1,
			"date" : date
		}

		return render(request,"stock/accounts.html",context)
	else:
		raise Http404

def stock_edit(request):
	if request.user.is_authenticated() and request.user.is_staff:
		itemname = request.session.get('itemname')
		start_dt = request.session.get('start_dt')
		end_dt = datetime.date.today()
		date  = datetime.date.today()
		queryset = StockDetails.objects.filter(itemname=itemname,date__range=[start_dt,end_dt]).order_by('date')
		total_incoming = StockDetails.objects.filter(itemname=itemname,date__range=[start_dt,end_dt]).aggregate(Sum('incoming'))
		total_sales = StockDetails.objects.filter(itemname=itemname,date__range=[start_dt,end_dt]).aggregate(Sum('sales'))
		list_dict = [{'date': date,'itemname':itemname ,'incoming':'0.000','sales':'0.000'} for x in range(len(queryset)+1) ]
		StockDetailsEditFormSet = modelformset_factory(StockDetails,form=StockDetailsEditForm,extra=1,max_num=1000)
		EditFormSet = StockDetailsEditFormSet(request.POST or None, queryset=queryset, initial=list_dict)

		if EditFormSet.is_valid() :
			if not request.user.is_active:
				return redirect('home')
			datelist = []
			for forms in EditFormSet:
				rec_dt = forms.cleaned_data.get('date')
				if rec_dt not in datelist:
					datelist.append(rec_dt)
				else:
					messages.warning(request, str(rec_dt) +' has more than 1 record!')
					return redirect('stock_edit')
			
			if (len(queryset) > 0):
				models = EditFormSet.save(commit=False)
				models = sorted(models, key=attrgetter('date'))
				if models[0].date < queryset[0].date:
					messages.warning(request, str(models[0].date) +' should be greater than first date')
					return redirect('stock_edit')
				models[0].closing = models[0].opening+models[0].incoming-models[0].sales
				models[0].save()
				
				for model in models[1:len(models)]:
					model.opening = models[models.index(model)-1].closing
					model.closing = model.opening+model.incoming-model.sales
					model.save()
			else:
				EditFormSet.save()
	            
			messages.success(request, 'successfully edited!')
			return redirect('stock_edit')
		
		context = {
			"logged_in_as": "You are logged in as admin",
			"formset" : EditFormSet,
			"total_incoming":total_incoming['incoming__sum'],
			"total_sales":total_sales['sales__sum'],
		}

		return render(request,"stock/stockedit.html",context)
	else:
		raise Http404

def stock_view(request):
	if request.user.is_authenticated() and request.user.is_staff:
		itemname = request.session.get('itemname')
		start_dt = request.session.get('start_dt')
		end_dt = request.session.get('end_dt')

		queryset = StockDetails.objects.filter(itemname=itemname,date__range=[start_dt,end_dt]).order_by('date')
		

		total_incoming = StockDetails.objects.filter(itemname=itemname,date__range=[start_dt,end_dt]).aggregate(Sum('incoming'))
		total_sales = StockDetails.objects.filter(itemname=itemname,date__range=[start_dt,end_dt]).aggregate(Sum('sales'))
		
		context = {
			"logged_in_as" : "You are logged in as admin",
			"itemname" : itemname,
			"queryset" : queryset,
			"total_incoming" : total_incoming['incoming__sum'],
			"total_sales" : total_sales['sales__sum'],
		}

		if 'Print' in request.POST:
			pdf = render_to_pdf('stockitempdf.html', context)
			if pdf:
				response = HttpResponse(pdf, content_type='application/pdf')
				filename = "stock_of_%s.pdf" %(itemname)
				content = "inline; filename='%s'" %(filename)
				download = request.GET.get("download")
				if download:
					content = "attachment; filename='%s'" %(filename)
				response['Content-Disposition'] = content
				return response
			return HttpResponse("Not found")

		return render(request,"stock/stockview.html",context)
	else:
		raise Http404

def tax_view(request,per=100):
	if request.user.is_authenticated() and request.user.is_staff:
		per=Decimal(Decimal(per)/100)
		month_start = request.session.get('tax_mon').split('-')
		ss_rate = Decimal(request.session.get('ss_rate'))
		sg_rate = Decimal(request.session.get('sg_rate'))
		is_rate = Decimal(request.session.get('is_rate'))
		ig_rate = Decimal(request.session.get('ig_rate'))
		g_cgst = Decimal(request.session.get('g_cgst'))/100
		g_sgst = Decimal(request.session.get('g_sgst'))/100
		s_cgst = Decimal(request.session.get('s_cgst'))/100
		s_sgst = Decimal(request.session.get('s_sgst'))/100

		last_day = list(calendar.monthrange(int(month_start[0]),int(month_start[1])))
		month_end = datetime.date(int(month_start[0]),int(month_start[1]),int(last_day[1]))
		month_start = datetime.date(int(month_start[0]),int(month_start[1]),1)

		form = None
		# itemname = request.session.get('itemname')
		# start_dt = request.session.get('start_dt')
		# end_dt = request.session.get('end_dt')

		
		queryset = StockDetails.objects.select_related('itemname').filter(date__range=[month_start,month_end])
		#StockDetails.objects.select_related('itemname').filter(date__range=[month_start,month_end]).aggregate(Sum('incoming'))
		# queryset = StockDetails.objects.filter(
		 #    ItemDetails__Itemtype='g',
		 #    #ItemDetails__name__icontains='mozzarella',
		# ).filter(date__range=[month_start,month_end])
		# for rec in queryset:
		# 	if rec.itemname.itemtype in ('g','G'):
		# 		print rec.sales
		sal_sil_weight_gt = Decimal('0.000')
		sal_gol_weight_gt = Decimal('0.000') 
		inc_sil_weight_gt = Decimal('0.000')
		inc_gol_weight_gt = Decimal('0.000')
		bill_no = sorted(random.sample(range(10,1000),int(last_day[1])*2))
		s_billno = bill_no[0:][::2]
		g_billno = bill_no[1:][::2]
		tax_dict=[]
		for dt in daterange(month_start,month_end):
			sal_sil_weight = Decimal(sum([rec.sales for rec in queryset if rec.itemname.itemtype in ('s','S') and rec.date == dt])*per)
			sal_sil_weight_gt += sal_sil_weight 
			sal_sil_amount = "{0:.2f}".format(sal_sil_weight * ss_rate)
			sal_sil_cgst = "{0:.2f}".format(Decimal(sal_sil_amount) * s_cgst)
			sal_sil_sgst = "{0:.2f}".format(Decimal(sal_sil_amount) * s_sgst)
			sal_sil_total = sum([Decimal(sal_sil_amount), Decimal(sal_sil_cgst), Decimal(sal_sil_sgst)])

			sal_gol_weight = Decimal(sum([rec.sales for rec in queryset if rec.itemname.itemtype in ('g','G') and rec.date == dt])*per)
			sal_gol_weight_gt += sal_gol_weight
			sal_gol_amount = "{0:.2f}".format(sal_gol_weight * sg_rate)
			sal_gol_cgst = "{0:.2f}".format(Decimal(sal_gol_amount) * g_cgst)
			sal_gol_sgst = "{0:.2f}".format(Decimal(sal_gol_amount) * g_sgst)
			sal_gol_total = sum([Decimal(sal_gol_amount), Decimal(sal_gol_cgst), Decimal(sal_gol_sgst)])

			inc_sil_weight = Decimal(sum([rec.incoming for rec in queryset if rec.itemname.itemtype in ('s','S') and rec.date == dt])*per)
			inc_sil_weight_gt += inc_sil_weight
			inc_sil_amount = "{0:.2f}".format(inc_sil_weight * is_rate)
			inc_sil_cgst = "{0:.2f}".format(Decimal(inc_sil_amount) * s_cgst)
			inc_sil_sgst = "{0:.2f}".format(Decimal(inc_sil_amount) * s_sgst)
			inc_sil_total = sum([Decimal(inc_sil_amount), Decimal(inc_sil_cgst), Decimal(inc_sil_sgst)])

			inc_gol_weight = Decimal(sum([rec.incoming for rec in queryset if rec.itemname.itemtype in ('g','G') and rec.date == dt])*per)
			inc_gol_weight_gt += inc_gol_weight
			inc_gol_amount = "{0:.2f}".format(inc_gol_weight * ig_rate)
			inc_gol_cgst = "{0:.2f}".format(Decimal(inc_gol_amount) * g_cgst)
			inc_gol_sgst = "{0:.2f}".format(Decimal(inc_gol_amount) * g_sgst)
			inc_gol_total = sum([Decimal(inc_gol_amount), Decimal(inc_gol_cgst), Decimal(inc_gol_sgst)])
			
			tax_dict.append({'date': dt,
						's_bill_no': s_billno[dt.day-1],
						'g_bill_no': g_billno[dt.day-1],
						'sal_sil_weight':sal_sil_weight,
						'sal_sil_amount':sal_sil_amount,
						'sal_gol_weight':sal_gol_weight,
						'sal_gol_amount':sal_gol_amount,
						'inc_sil_weight':inc_sil_weight,
						'inc_sil_amount':inc_sil_amount,
						'inc_gol_weight':inc_gol_weight,
						'inc_gol_amount':inc_gol_amount,
						'sal_sil_cgst':sal_sil_cgst,
						'sal_sil_sgst':sal_sil_sgst,
						'sal_gol_cgst':sal_gol_cgst,
						'sal_gol_sgst':sal_gol_sgst,
						'inc_sil_cgst':inc_sil_cgst,
						'inc_sil_sgst':inc_sil_sgst,
						'inc_gol_cgst':inc_gol_cgst,
						'inc_gol_sgst':inc_gol_sgst,
						'sal_sil_total':sal_sil_total,
						'sal_gol_total':sal_gol_total,
						'inc_sil_total':inc_sil_total,
						'inc_gol_total':inc_gol_total,
						})

		sal_sil_amount_gt = Decimal("{0:.2f}".format(sal_sil_weight_gt * ss_rate))
		sal_sil_cgst_gt = Decimal("{0:.2f}".format(Decimal(sal_sil_amount_gt) * s_cgst))
		sal_sil_sgst_gt = Decimal("{0:.2f}".format(Decimal(sal_sil_amount_gt) * s_sgst))
		sal_sil_total_gt = sum([sal_sil_amount_gt, sal_sil_cgst_gt, sal_sil_sgst_gt])

		sal_gol_amount_gt = Decimal("{0:.2f}".format(sal_gol_weight_gt * sg_rate))
		sal_gol_cgst_gt = Decimal("{0:.2f}".format(Decimal(sal_gol_amount_gt) * g_cgst))
		sal_gol_sgst_gt = Decimal("{0:.2f}".format(Decimal(sal_gol_amount_gt) * g_sgst))
		sal_gol_total_gt = sum([sal_gol_amount_gt, sal_gol_cgst_gt, sal_gol_sgst_gt])

		inc_sil_amount_gt = Decimal("{0:.2f}".format(inc_sil_weight_gt * is_rate))
		inc_sil_cgst_gt = Decimal("{0:.2f}".format(Decimal(inc_sil_amount_gt) * s_cgst))
		inc_sil_sgst_gt = Decimal("{0:.2f}".format(Decimal(inc_sil_amount_gt) * s_sgst))
		inc_sil_total_gt = sum([inc_sil_amount_gt, inc_sil_cgst_gt, inc_sil_sgst_gt])

		inc_gol_amount_gt = Decimal("{0:.2f}".format(inc_gol_weight_gt * ig_rate))
		inc_gol_cgst_gt = Decimal("{0:.2f}".format(Decimal(inc_gol_amount_gt) * g_cgst))
		inc_gol_sgst_gt = Decimal("{0:.2f}".format(Decimal(inc_gol_amount_gt) * g_sgst))
		inc_gol_total_gt = sum([inc_gol_amount_gt, inc_gol_cgst_gt, inc_gol_sgst_gt])
		
		sales_sum = sal_sil_amount_gt+sal_gol_amount_gt
		incoming_sum = inc_sil_amount_gt+inc_gol_amount_gt
		cgst_sum = inc_sil_cgst_gt+inc_gol_cgst_gt+sal_sil_cgst_gt+sal_gol_cgst_gt
		sgst_sum = inc_sil_sgst_gt+inc_gol_sgst_gt+sal_sil_sgst_gt+sal_gol_sgst_gt
		total_sum = inc_sil_total_gt+inc_gol_total_gt+sal_sil_total_gt+sal_gol_total_gt
		sales_sum
		incoming_sum
		cgst_sum
		sgst_sum
		total_sum
		grand_tot={
		'sal_sil_weight_gt':sal_sil_weight_gt,
		'sal_sil_amount_gt':sal_sil_amount_gt,
		'sal_gol_weight_gt':sal_gol_weight_gt,
		'sal_gol_amount_gt':sal_gol_amount_gt,
		'inc_sil_weight_gt':inc_sil_weight_gt,
		'inc_sil_amount_gt':inc_sil_amount_gt,
		'inc_gol_weight_gt':inc_gol_weight_gt,
		'inc_gol_amount_gt':inc_gol_amount_gt,
		'sal_sil_cgst_gt':sal_sil_cgst_gt,
		'sal_sil_sgst_gt':sal_sil_sgst_gt,
		'sal_gol_cgst_gt':sal_gol_cgst_gt,
		'sal_gol_sgst_gt':sal_gol_sgst_gt,
		'inc_sil_cgst_gt':inc_sil_cgst_gt,
		'inc_sil_sgst_gt':inc_sil_sgst_gt,
		'inc_gol_cgst_gt':inc_gol_cgst_gt,
		'inc_gol_sgst_gt':inc_gol_sgst_gt,
		'sal_sil_total_gt':sal_sil_total_gt,
		'sal_gol_total_gt':sal_gol_total_gt,
		'inc_sil_total_gt':inc_sil_total_gt,
		'inc_gol_total_gt':inc_gol_total_gt,
		}
		days_br = {
		28:10,
		29:9,
		30:8,
		31:7
		}
		context = {
			"logged_in_as" : "You are logged in as admin",
			"tax_dict" : tax_dict,
			"grand_tot" : grand_tot,
			"sales_sum":sales_sum,
			"incoming_sum":incoming_sum,
			"cgst_sum":cgst_sum,
			"sgst_sum":sgst_sum,
			"total_sum":total_sum,
			"days_br":range(days_br[last_day[1]]),
		}
		
		if 'Print' in request.POST:
			pdf = render_to_pdf('stock/taxpdf.html', context)
			if pdf:
				response = HttpResponse(pdf, content_type='application/pdf')
				filename = "taxview.pdf"
				content = "inline; filename='%s'" %(filename)
				download = request.GET.get("download")
				if download:
					content = "attachment; filename='%s'" %(filename)
				response['Content-Disposition'] = content
				return response
			return HttpResponse("Not found")

		return render(request,"stock/taxview.html",context)
	else:
		raise Http404