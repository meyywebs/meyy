# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import datetime
# Create your models here.
class ItemDetails (models.Model):
	"""docstring for ItemDetails """
	S = 'S'
	G = 'G'
	choice = ((S,'Silver'),(G,'Gold'),)
	name = models.CharField(primary_key=True,max_length=120,blank=False)
	description = models.TextField()
	itemtype = models.CharField(max_length=1,choices=choice)
	kdm = models.BooleanField(default=False)
	active = models.BooleanField(default=True)
	opening = models.DecimalField(null=False, blank=True, max_digits=10, default=0.000,decimal_places=3)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	#StockDetails = models.ManyToManyField('StockDetails')
	# def __init__(self, arg):
	# 	super(ItemDetails , self).__init__()
	# 	self.arg = arg
	def details(self):
		return {'name':self.name,
				'description':self.description,
				'itemtype':self.itemtype,
				'kdm':self.kdm,
				'active':self.active,
				'opening':self.opening
				}
	def __unicode__(self):#def __str__(self):
		return self.name

class StockDetails (models.Model):
	"""docstring for ItemDetails """
	date = models.DateField(null=False)
	itemname = models.ForeignKey(ItemDetails)
	#itemid = models.OneToOneField(ItemDetails)
	#models.DecimalField(..., max_digits=5, decimal_places=2)
	incoming = models.DecimalField(null=False, blank=True, max_digits=10, default=0.000,decimal_places=3)
	opening = models.DecimalField(null=False, blank=True, max_digits=10, default=0.000,decimal_places=3)
	sales = models.DecimalField(null=False, blank=True,max_digits=10, default=0.000, decimal_places=3)
	closing = models.DecimalField(null=False, blank=True, max_digits=10, default=0.000,decimal_places=3)
	#description = models.TextField(null=True, blank=True, default='')
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

	# def __init__(self, arg):
	# 	super(ItemDetails , self).__init__()
	# 	self.arg = arg

	def __unicode__(self):#def __str__(self):
		obj_str = str(self.itemname) +' | '+ str(self.date)
		return obj_str
		