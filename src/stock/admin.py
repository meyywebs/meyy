# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import ItemDetails,StockDetails

class StockDetailsTabularInline(admin.TabularInline):
	model = StockDetails

class ItemDetailsAdmin(admin.ModelAdmin):
	list_display = ["__unicode__","timestamp"]
	#inlines = [StockDetailsTabularInline]
	class Meta:
		"""docstring for Meta"""
		model = ItemDetails
			
admin.site.register(ItemDetails,ItemDetailsAdmin)
admin.site.register(StockDetails)