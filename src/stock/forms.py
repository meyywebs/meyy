from django import forms
from django.forms.formsets import BaseFormSet
from django.forms import ModelChoiceField
from .models import ItemDetails,StockDetails
from django.core.exceptions import ObjectDoesNotExist
import datetime
from decimal import Decimal
import re

class ItemDetailsEditForm(forms.ModelForm):
	opening = forms.CharField()

	class Meta:
 		model = ItemDetails
 		fields = ['description','itemtype','kdm','active','opening']

 	def clean_opening(self):
		opening = self.cleaned_data.get('opening')
		match = re.match("^\d+\.\d{3}$",opening)
		if not match:
			raise forms.ValidationError("opening format X+.XXX")
		return opening

	def clean_kdm(self):
		kdm = self.cleaned_data.get('kdm')
		itemtype = self.cleaned_data.get('itemtype')
		if itemtype == 'S' and kdm == True:
			raise forms.ValidationError("silver cannot be kdm")
		return kdm

class ItemDetailsForm(forms.ModelForm):
	opening = forms.CharField()

 	class Meta:
 		model = ItemDetails
 		fields = ['name','description','itemtype','kdm','active','opening']

	def clean_opening(self):
		opening = self.cleaned_data.get('opening')
		match = re.match("^\d+\.\d{3}$",opening)
		if not match:
			raise forms.ValidationError("opening format X+.XXX")
		return opening

	def clean_kdm(self):
		kdm = self.cleaned_data.get('kdm')
		itemtype = self.cleaned_data.get('itemtype')
		if itemtype == 'S'and kdm == True:
			raise forms.ValidationError("silver cannot be kdm")
		return kdm

class StockDetailsForm(forms.ModelForm):
	itemname = forms.ModelChoiceField(queryset = ItemDetails.objects.values_list('name',flat=True).filter(active=True))
	date = forms.DateField()
	incoming = forms.CharField()
	sales = forms.CharField()
	opening = forms.CharField(initial='0.000', widget = forms.HiddenInput(), required = False)
	closing = forms.CharField(initial='0.000', widget = forms.HiddenInput(), required = False)
	date.disabled = True
	class Meta:
 		model = StockDetails
 		fields = ['date', 'itemname', 'incoming', 'opening', 'sales' ,'closing']

	def clean_itemname(self):
		name = self.cleaned_data.get('itemname')
		itemname = ItemDetails.objects.filter(name = name)
		if not itemname[0] :
			raise forms.ValidationError("no itemname "+name)
		return itemname[0]

	def clean_incoming(self):
		incoming = self.cleaned_data.get('incoming')
		match = re.match("^\d+\.\d{3}$",incoming)
		if not match:
			raise forms.ValidationError("incoming format X+.XXX")
		return incoming

	def clean_sales(self):
		sales = self.cleaned_data.get('sales')
		match = re.match("^\d+\.\d{3}$",sales)
		if not match:
			raise forms.ValidationError("sales format X+.XXX")
		return sales

	def clean_opening(self):
		name = self.cleaned_data.get('itemname')
		date = self.cleaned_data.get('date')
		
		try:
			sd_obj = StockDetails.objects.filter(itemname=name, date__lt=date).latest('date')
			yest_closing = sd_obj.closing
		except ObjectDoesNotExist:
			try:
				sd_obj = ItemDetails.objects.get(name = name)
				yest_closing = sd_obj.opening
			except ObjectDoesNotExist:
				yest_closing = '0.000'
		opening = str(yest_closing)
		return opening

	def clean_closing(self):
		incoming = self.cleaned_data.get('incoming')
		sales = self.cleaned_data.get('sales')
		opening = self.cleaned_data.get('opening')
		try:
			closing = Decimal(opening) + Decimal(incoming) - Decimal(sales)
			return str(closing)
		except TypeError:
			return '0.000'

	def clean_date(self):
		date = self.cleaned_data.get('date')
		if not date:
			raise forms.ValidationError("date is needed")
		return date

class StockDetailsEditForm(forms.ModelForm):

	itemname = forms.ModelChoiceField(queryset = ItemDetails.objects.values_list('name',flat=True).filter(active=True))
	itemname.disabled = True
	date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ),)
	incoming = forms.CharField()
	sales = forms.CharField()
	opening = forms.CharField(initial='0.000')
	closing = forms.CharField(initial='0.000')

	def __init__(self, *args, **kwargs):
		super(StockDetailsEditForm, self).__init__(*args, **kwargs)
		self.fields['opening'].widget.attrs['readonly'] = True
		self.fields['closing'].widget.attrs['readonly'] = True
	class Meta:
 		model = StockDetails
 		fields = ['date', 'itemname', 'incoming', 'opening', 'sales' ,'closing']

	def clean_itemname(self):
		name = self.cleaned_data.get('itemname')
		itemname = ItemDetails.objects.filter(name = name)
		if not itemname[0] :
			raise forms.ValidationError("no itemname "+name)
		return itemname[0]


	def clean_incoming(self):
		incoming = self.cleaned_data.get('incoming')
		match = re.match("^\d+\.\d{3}$",incoming)
		if not match:
			raise forms.ValidationError("incoming format X+.XXX")
		return incoming

	def clean_sales(self):
		sales = self.cleaned_data.get('sales')
		match = re.match("^\d+\.\d{3}$",sales)
		if not match:
			raise forms.ValidationError("sales format X+.XXX")
		return sales

	def clean_opening(self):
		return self.cleaned_data.get('opening')

	def clean_closing(self):

		return self.cleaned_data.get('closing')
	def clean_date(self):
		date = self.cleaned_data.get('date')
		if not date:
			raise forms.ValidationError("date is needed")
		return date
