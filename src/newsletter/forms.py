from django import forms
from bootstrap_datepicker.widgets import DatePicker
from .models import SignUp
from stock.models import StockDetails
import re
import datetime

class TaxDetailsForm(forms.Form):
	tax_month = forms.DateField(initial=datetime.date.today(),widget=forms.DateInput(format='%Y-%m'), input_formats=('%Y-%m',))
	sales_silver_rate = forms.CharField(initial ='0.00')
	sales_gold_rate = forms.CharField(initial ='0.00')
	incoming_silver_rate = forms.CharField(initial ='0.00')
	incoming_gold_rate = forms.CharField(initial ='0.00')
	gold_cgst = forms.CharField(initial ='1.5')
	gold_sgst = forms.CharField(initial ='1.5')
	silver_cgst = forms.CharField(initial ='1.5')
	silver_sgst = forms.CharField(initial ='1.5')
	def clean_sales_silver_rate(self):
		sales_silver_rate = self.cleaned_data.get('sales_silver_rate')
		match = re.match("^\d+\.\d{2}$",sales_silver_rate)
		if not match:
			raise forms.ValidationError("rupees.paisa format X+.XX")
		return sales_silver_rate

	def clean_sales_gold_rate(self):
		sales_gold_rate = self.cleaned_data.get('sales_gold_rate')
		match = re.match("^\d+\.\d{2}$",sales_gold_rate)
		if not match:
			raise forms.ValidationError("rupees.paisa format X+.XX")
		return sales_gold_rate

	def clean_incoming_silver_rate(self):
		incoming_silver_rate = self.cleaned_data.get('incoming_silver_rate')
		match = re.match("^\d+\.\d{2}$",incoming_silver_rate)
		if not match:
			raise forms.ValidationError("rupees.paisa format X+.XX")
		return incoming_silver_rate

	def clean_incoming_gold_rate(self):
		incoming_gold_rate = self.cleaned_data.get('incoming_gold_rate')
		match = re.match("^\d+\.\d{2}$",incoming_gold_rate)
		if not match:
			raise forms.ValidationError("rupees.paisa format X+.XX")
		return incoming_gold_rate

	def clean_gold_cgst(self):
		gold_cgst = self.cleaned_data.get('gold_cgst')
		match = re.match("^\d{1,2}\.\d+$",gold_cgst)
		if not match:
			raise forms.ValidationError("percentage format XX.X+")
		return gold_cgst

	def clean_gold_sgst(self):
		gold_sgst = self.cleaned_data.get('gold_sgst')
		match = re.match("^\d{1,2}\.\d+$",gold_sgst)
		if not match:
			raise forms.ValidationError("percentage format XX.X+")
		return gold_sgst

	def clean_silver_cgst(self):
		silver_cgst = self.cleaned_data.get('silver_cgst')
		match = re.match("^\d{1,2}\.\d+$",silver_cgst)
		if not match:
			raise forms.ValidationError("percentage format XX.X+")
		return silver_cgst

	def clean_silver_sgst(self):
		silver_sgst = self.cleaned_data.get('silver_sgst')
		match = re.match("^\d{1,2}\.\d+$",silver_sgst)
		if not match:
			raise forms.ValidationError("percentage format XX.X+")
		return silver_sgst

class CustomForm(forms.Form):
	full_name = forms.CharField()
	email = forms.EmailField()
	message  = forms.CharField()

class SignUpForm(forms.ModelForm):
	class Meta:
		model = SignUp
		fields = ['full_name','email']

	def clean_email(self):
		email = self.cleaned_data.get('email')
		email_base,provider = email.split("@")
		domain,extension = provider.split(".")

		if not domain == 'gmail':
			raise forms.ValidationError("accepts only gmail")
		if not extension == 'com':
			raise forms.ValidationError("accepts only .com")

		return email

	def clean_full_name(self):
		full_name = self.cleaned_data.get('full_name')
		name_check = re.compile(r"[^A-Za-zs.]")

		if name_check.search(full_name):
			raise forms.ValidationError("accepts only characters")

		return full_name

class StockDateForm(forms.Form):
	date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ),initial=datetime.date.today())
	def __init__(self, *args, **kwargs):
		super(StockDateForm, self).__init__(*args, **kwargs)
		self.fields['date'].label = ""

	def clean_date(self):
		date = self.cleaned_data.get('date')
		if (date > datetime.date.today()):
			raise forms.ValidationError("future date is restricted")
		return date

class StockEditForm(forms.ModelForm):
	start_dt = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ),)
	end_dt = forms.DateField(initial=datetime.date.today(),widget=forms.TextInput(attrs={'type': 'date'} ),)
	
	class Meta:
 		model = StockDetails
 		fields = ['itemname',]

	def __init__(self, *args, **kwargs):
		super(StockEditForm, self).__init__(*args, **kwargs)
		self.fields['start_dt'].label = ""
		self.fields['end_dt'].label = ""
	def clean_start_dt(self):
		start_dt = self.cleaned_data.get('start_dt')
		if not start_dt or (start_dt >= datetime.date.today()):
			raise forms.ValidationError("start date should be less than today")
			return start_dt
		return start_dt
	def clean_end_dt(self):
		end_dt = self.cleaned_data.get('end_dt')
		start_dt = self.cleaned_data.get('start_dt')
		if not end_dt or (end_dt > datetime.date.today()) or (start_dt > end_dt):
			raise forms.ValidationError("start date < end date < or = today")
			return start_dt
		return end_dt
	# def clean_itemname(self):
	# 	item_name = self.cleaned_data.get('item_name')
	# 	start_dt = self.cleaned_data.get('start_dt')
	# 	if not end_dt or (end_dt > datetime.date.today()) or (start_dt > end_dt):
	# 		raise forms.ValidationError("start date < end date < or = today")
	# 		return start_dt
	# 	return item_name

