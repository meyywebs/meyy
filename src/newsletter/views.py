# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.mail import send_mail
from django.shortcuts import render,Http404
from .forms import CustomForm, SignUpForm
from django.contrib import messages
from django.shortcuts import redirect
from stock.models import ItemDetails,StockDetails
from django.conf import settings
from django.contrib.auth.models import User
from .forms import StockDateForm,StockEditForm,TaxDetailsForm
from django.db.models import Sum
from django.http import HttpResponse
from django.views.generic import View
from .utils import render_to_pdf
import calendar
import datetime
# Create your views here.
def contact (request):
	title = 'Contact Us'
	text_align_center = True
	form = CustomForm(request.POST or None)
	context={
		"form": form,
		"title": title,
		"text_align_center": text_align_center,
	}
	if form.is_valid():
		if not request.user.is_active:
			return redirect('home')
		# for key,value in form.cleaned_data.iteritems():
		# 	print key,value
		email = form.cleaned_data.get('email')
		full_name = form.cleaned_data.get('full_name')
		message = full_name +' : '+form.cleaned_data.get('message') +' ||for more : '+email
		to_email = ['vishnu.thg@gmail.com']
		send_mail('From contact us', message, settings.EMAIL_HOST_USER, to_email, fail_silently=False)
	return render(request,"custom.html",context)

def home (request,date=datetime.date.today()):
	title = 'Welcome'
	request.session['REGISTRATION_AUTO_LOGIN'] = False
	if request.session.get('date_updated'):
		date = request.session.get('date_updated')
	form = StockDateForm(request.POST or None)
	edit_form = StockEditForm(request.POST or None)
	context = {
		"template_title":title,
		"form":form,
		"edit_form":edit_form,
			}
	if request.user.is_authenticated() and request.user.is_staff:

		if edit_form.is_valid():
			if not request.user.is_active:
				return redirect('home')
			request.session['itemname'] = str(edit_form.cleaned_data.get('itemname'))
			request.session['start_dt'] = str(edit_form.cleaned_data.get('start_dt'))
			request.session['end_dt'] = str(edit_form.cleaned_data.get('end_dt'))
			if 'View Stock' in request.POST:
				return redirect('stock_view')
			return redirect('stock_edit')

		elif 'Print' in request.POST:
			queryset = StockDetails.objects.all().filter(date=date).order_by('timestamp')
			total_incoming = StockDetails.objects.filter(date=date).aggregate(Sum('incoming'))
			total_sales = StockDetails.objects.filter(date=date).aggregate(Sum('sales'))
			data = {
	             	"queryset":queryset,
					"total_incoming":total_incoming['incoming__sum'],
					"total_sales":total_sales['sales__sum'],
					"date":date
	         		}
			pdf = render_to_pdf('stockpdf.html', data)
			#return HttpResponse(pdf, content_type='application/pdf')
			if pdf:
				response = HttpResponse(pdf, content_type='application/pdf')
				filename = "stock_of_%s.pdf" %(date)
				content = "inline; filename='%s'" %(filename)
				download = request.GET.get("download")
				if download:
					content = "attachment; filename='%s'" %(filename)
				response['Content-Disposition'] = content
				return response
			return HttpResponse("Not found")
		
		elif form.is_valid():
			if not request.user.is_active:
				return redirect('home')
			date = form.cleaned_data.get('date')
			return redirect('daily_stock',1,date)
		
		queryset = StockDetails.objects.all().filter(date=date).order_by('timestamp')
		total_incoming = StockDetails.objects.filter(date=date).aggregate(Sum('incoming'))
		total_sales = StockDetails.objects.filter(date=date).aggregate(Sum('sales'))
		workers = User.objects.all().filter(is_staff=False,is_active=True).order_by('-date_joined')
		context = {
			"form":form,
			"logged_in_as": "You are logged in as admin",
			"queryset":queryset,
			"total_incoming":total_incoming['incoming__sum'],
			"total_sales":total_sales['sales__sum'],
			"date":date,
			"workers":workers,
			"edit_form":edit_form,
		}
			#return render(request,"stock/home.html",context)
	return render(request,"home.html",context)

def tax_detail(request):
	if request.user.is_authenticated() and request.user.is_staff:
		TaxDetailForm = TaxDetailsForm(request.POST or None)
		if TaxDetailForm.is_valid():
				if not request.user.is_active:
					return redirect('home')
				request.session['tax_mon'] = str(TaxDetailForm.cleaned_data.get('tax_month'))
				request.session['ss_rate'] = TaxDetailForm.cleaned_data.get('sales_silver_rate')
				request.session['sg_rate'] = TaxDetailForm.cleaned_data.get('sales_gold_rate')
				request.session['is_rate'] = TaxDetailForm.cleaned_data.get('incoming_silver_rate')
				request.session['ig_rate'] = TaxDetailForm.cleaned_data.get('incoming_gold_rate')
				request.session['g_cgst'] = TaxDetailForm.cleaned_data.get('gold_cgst')
				request.session['g_sgst'] = TaxDetailForm.cleaned_data.get('gold_sgst')
				request.session['s_cgst'] = TaxDetailForm.cleaned_data.get('silver_cgst')
				request.session['s_sgst'] = TaxDetailForm.cleaned_data.get('silver_sgst')

				return redirect('tax_view')
		context = {
		"logged_in_as" : "you are logged in as admin",
		"TaxDetailsForm" : TaxDetailForm,
			}
		return render(request,"tax_detail.html",context)
	else:
		raise Http404
