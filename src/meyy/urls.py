"""meyy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url
from django.contrib import admin
from stock import stockviews
from newsletter import views
from meyy import aboutviews
urlpatterns = [
	url(r'^$', views.home, name='home'),
    url(r'^stock/$', stockviews.stock_home, name='stock_home'),
    url(r'^stockedit/$', stockviews.stock_edit, name='stock_edit'),
    url(r'^stockview/$', stockviews.stock_view, name='stock_view'),
    url(r'^dailystock/(?P<param>.*)/(?P<date>.*)/$', stockviews.daily_stock, name='daily_stock'),
    url(r'^dailystock/$', stockviews.daily_stock, name='daily_stock'),
    url(r'^taxdetail/$', views.tax_detail, name='tax_detail'),
    url(r'^taxview/$', stockviews.tax_view, name='tax_view'),
    url(r'^taxview/(?P<per>[1-9][0-9]|[1-9])/$', stockviews.tax_view, name='tax_view'),
	url(r'^contact/$', views.contact, name='contact'),
	url(r'^about/$', aboutviews.about, name='about'),
    url(r'^stock/itemedit/(?P<param>.*)/$', stockviews.item_edit, name='itemedit'),
    url(r'^admin/', admin.site.urls) ,
    url(r'^accounts/', include('registration.backends.simple.urls')),
]

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
